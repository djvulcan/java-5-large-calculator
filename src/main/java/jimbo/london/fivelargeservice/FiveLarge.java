package jimbo.london.fivelargeservice;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class FiveLarge {
    public LocalDate NextFiveLargeDate;
    private LocalDateTime NextFiveLargeDateTime;
    public LocalDate CurrentDate = LocalDate.now();
    private static int FiveLargeDayOfMonth = 25;
    public int FiveLargeDays;
    public int FiveLargeHours;
    public int FiveLargeMinutes;
    public int FiveLargeSeconds;
    private Duration FiveLargeDelta;
    
    {
        GetNextFiveLargeDate();
    }
    
    public void GetNextFiveLargeDate() {
        int CurrentYear = CurrentDate.getYear();
        int CurrentDay = CurrentDate.getDayOfMonth();
        int CurrentMonth = CurrentDate.getMonthValue();
        CurrentMonth += (FiveLargeDayOfMonth <= CurrentDay) ? 1 :  0;
        NextFiveLargeDate = LocalDate.of(CurrentYear, CurrentMonth, FiveLargeDayOfMonth);
        AdjustWeekend();
        if (NextFiveLargeDate.isEqual(CurrentDate)) {
            CurrentMonth ++;
            NextFiveLargeDate = LocalDate.of(CurrentYear, CurrentMonth, FiveLargeDayOfMonth);
            AdjustWeekend();
        }
        NextFiveLargeDateTime = NextFiveLargeDate.atStartOfDay();
    }

    private void AdjustWeekend() {
        int FiveLargeDayOfWeek = NextFiveLargeDate.getDayOfWeek().getValue();
        if (FiveLargeDayOfWeek > 5) {
            this.NextFiveLargeDate = NextFiveLargeDate.minusDays(FiveLargeDayOfWeek-5);
        }
    }

    public void CalculateDeltas() {
        FiveLargeDelta = Duration.between(this.NextFiveLargeDateTime, LocalDateTime.now());
        FiveLargeDays = CalculateDays();
        FiveLargeHours = CalculateHours();
        FiveLargeMinutes = CalculateMinutes();
        FiveLargeSeconds = CalculateSeconds();
    }

    private int CalculateDays() {
        long diff = Math.abs(FiveLargeDelta.toDays());
        return (int)diff;
    }

    private int CalculateHours() {
        long diff = Math.abs((FiveLargeDelta.getSeconds()/3600)%24);
        return (int)diff;
    }

    private int CalculateMinutes() {
        long diff = Math.abs((FiveLargeDelta.getSeconds()/60)%60);
        return (int)diff;
    }
    
    private int CalculateSeconds() {
        long diff = Math.abs((FiveLargeDelta.getSeconds())%60);
        return (int)diff;
    }
}
