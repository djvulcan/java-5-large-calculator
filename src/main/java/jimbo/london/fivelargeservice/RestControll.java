package jimbo.london.fivelargeservice;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestControll {
    private final AtomicLong counter = new AtomicLong();

	@GetMapping("/")
	public RestResponse fivelarge() {
		return new RestResponse(counter.incrementAndGet());
	}
}