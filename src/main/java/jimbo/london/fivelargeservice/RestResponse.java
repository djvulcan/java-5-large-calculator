package jimbo.london.fivelargeservice;

import java.time.LocalDate;

public class RestResponse {
    private final long id;
    private final LocalDate FiveLargeDate;
    private final int FiveLargeDays;
    private final int FiveLargeHours;
    private final int FiveLargeMinutes;
    private final int FiveLargeSeconds;

	public RestResponse(long id) {
        this.id = id;
        FiveLarge myFiveLarge = new FiveLarge();
        myFiveLarge.CalculateDeltas();
        this.FiveLargeDate = myFiveLarge.NextFiveLargeDate;
        this.FiveLargeDays = myFiveLarge.FiveLargeDays;
        this.FiveLargeHours = myFiveLarge.FiveLargeHours;
        this.FiveLargeMinutes = myFiveLarge.FiveLargeMinutes;
        this.FiveLargeSeconds = myFiveLarge.FiveLargeSeconds;
	}

	public long getId() {
		return id;
	}

	public LocalDate getFiveLargeDate() {
		return FiveLargeDate;
    }
    
    public int getFiveLargeDays() {
        return FiveLargeDays;
    }

    public int getFiveLargeHours() {
        return FiveLargeHours;
    }

    public int getFiveLargeMinutes() {
        return FiveLargeMinutes;
    }

    public int getFiveLargeSeconds() {
        return FiveLargeSeconds;
    }
}