package jimbo.london.fivelargeservice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class FiveLargeTests {
	LocalDate TestFiveLargeDuringWeek = LocalDate.parse("2020-05-10");
	LocalDate TestFiveLargeOnWeekend = LocalDate.parse("2020-04-10");
	LocalDate TestFiveLargeEarlierInMonth = LocalDate.parse("2020-03-30");
	LocalDate TestFiveOnSameDayinWeekendShift = LocalDate.parse("2020-07-24");
	LocalDate TestFiveOnSameDayNonWeekend = LocalDate.parse("2020-08-25");

	@Test
	void TestFiveLargeFallsInWeek() {
		FiveLarge TestFiveLarge = new FiveLarge();
		TestFiveLarge.CurrentDate = TestFiveLargeDuringWeek;
		TestFiveLarge.GetNextFiveLargeDate();
		assertEquals(TestFiveLarge.NextFiveLargeDate, LocalDate.parse("2020-05-25"));
	}
	@Test
	void TestFiveLargeFallsOnWeekEnd() {
		FiveLarge TestFiveLarge = new FiveLarge();
		TestFiveLarge.CurrentDate = TestFiveLargeOnWeekend;
		TestFiveLarge.GetNextFiveLargeDate();
		assertEquals(TestFiveLarge.NextFiveLargeDate, LocalDate.parse("2020-04-24"));
	}

	@Test
	void TestFiveLargeWasEarlierThisMonth() {
		FiveLarge TestFiveLarge = new FiveLarge();
		TestFiveLarge.CurrentDate = TestFiveLargeEarlierInMonth;
		TestFiveLarge.GetNextFiveLargeDate();
		assertEquals(TestFiveLarge.NextFiveLargeDate, LocalDate.parse("2020-04-24"));
	}

	@Test
	void TestFiveLargeWasTodayAndWasWeekendShifted() {
		FiveLarge TestFiveLarge = new FiveLarge();
		TestFiveLarge.CurrentDate = TestFiveOnSameDayinWeekendShift;
		TestFiveLarge.GetNextFiveLargeDate();
		assertEquals(TestFiveLarge.NextFiveLargeDate, LocalDate.parse("2020-08-25"));
	}
	@Test
	void TestFiveLargeWasTodayAndNonWeekend() {
		FiveLarge TestFiveLarge = new FiveLarge();
		TestFiveLarge.CurrentDate = TestFiveOnSameDayNonWeekend;
		TestFiveLarge.GetNextFiveLargeDate();
		assertEquals(TestFiveLarge.NextFiveLargeDate, LocalDate.parse("2020-09-25"));
	}

}
